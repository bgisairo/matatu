<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {

	public function search_route_by_name($name)
	{
		$this->db->or_like('route_long_name', $name);
		$this->db->or_like('route_desc', $name);
		$q = $this->db->get('routes');
		
		if ($q->num_rows() > 0) {
			
			return $q;
		}else{
			return $result = false;
		}

	}
	public function search_by_route_number($rno)
	{
		$q = $this->db->get_where('routes',array('route_short_name'=>$rno));
		
		
		if ($q->num_rows() > 0) {
			
			return $q;
		}else{
			return $result = false;
		}
	}

}

/* End of file search_model.php */
/* Location: ./application/models/search_model.php */