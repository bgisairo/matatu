<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data= null;
		$data['routes'] = null;
		$this->load->view('search.php', $data, FALSE);
	}
	public function search()
	{
		$data=null;
		$phrase = $this->input->post('txt_search');
		$this->load->model('Search_model');
		if (is_numeric($phrase)) {
			$rs = $this->Search_model->search_by_route_number($phrase);
			// echo json_encode($rs->result()); #need to be in a json function
			if ($rs!=false) {
				$data['search'] = "Found ".count($rs->result())." results!";
				$data['routes'] = $rs->result();	
			}else{
				$data['search'] = "No results found!";
			}
			
		}else{
			$rs = $this->Search_model->search_route_by_name($phrase);
			if ($rs!=false) {
				$data['search'] = "Found ".count($rs->result())." results!";
				$data['routes'] = $rs->result();	
			}else{
				$data['search'] = "No results found!";
			}
			
		}
		$this->load->view('search.php', $data, FALSE);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */