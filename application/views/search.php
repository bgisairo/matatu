<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Matatu search</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>
	<div id="container" class="container">
		<div id="main" class="col-md-12">
			<div id="header" class="nav">
				
			</div>
			<div class="jumbotron center-block">
				<div class="form-group">
					<h2>Where to?</h2>
				</div>
				<div class="form-group">
					<form action="<?php echo site_url('home/search'); ?>" method="post" accept-charset="utf-8">
						<input class="form-control"type="search" name="txt_search" value="" placeholder="Unaenda?">
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-lg btn-block" type="submit" value="Search" name="btn_search"> <span class="glyphicon glyphicon-search"></span> Search</button>
					
					</div>
				</form>
				</div>	
			</div>

			
				<div class="center-block">
					<div class="title">
						<?php if (isset($search)): ?>
						<?php echo $search; ?>	
						<?php endif ?>
						
					</div>
					<?php if (isset($routes)): ?>
						<?php foreach ($routes as $row): ?>
							<h4><?php echo ($row->route_short_name); ?></h4>
							<p><?php echo $row->route_long_name;?></p>
							<p><?php echo $row->route_desc; ?></p>
						<?php endforeach ?>
					<?php endif ?>
			</div>
			
			
			<div id="footer" class="footer">
				
			</div>
		</div>
	</div>
</body>
</html>