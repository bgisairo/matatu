<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Matatu search results</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>
	<div id="container" class="container">
		<div id="main" class="row col-md-12">
			<div id="header" class="nav">
				
			</div>
			<div class="center-block">
				<?php foreach ($routes as $row): ?>
					<h4><?php echo ($row->route_short_name); ?></h4>
					<p><?php echo $row->route_long_name;?></p>
					<p><?php echo $row->route_desc; ?></p>
				<?php endforeach ?>
			</div>
			<div id="footer" class="footer">
				
			</div>
		</div>
	</div>

</body>
</html>